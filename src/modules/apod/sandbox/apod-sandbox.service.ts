import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APOD } from 'src/common/types/interface';
import { APODService } from '../../../common/services/apod/apod.service';

@Injectable()
export class ApodSandbox {
  constructor(private APODService: APODService) {}

  getRangeOfAPOD(start: string, end: string): Observable<APOD[]> {
    return this.APODService.getRangeOfAPOD(start, end);
  }

  getApod(value: string): Observable<APOD> {
    return this.APODService.getAPOD(value);
  }
}
