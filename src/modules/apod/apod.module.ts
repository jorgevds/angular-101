import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { APODComponent } from './containers/apod/apod.component';
import { ApodDetailComponent } from './containers/apod-detail/apod-detail.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { DateFormModule } from '../../common/modules/dateform/dateform.module';
import { ApodRoutingModule } from './apod-routing.module';
import { ApodSandbox } from './sandbox/apod-sandbox.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSkeletonLoaderModule,
    ApodRoutingModule,
    DateFormModule,
  ],
  declarations: [APODComponent, ApodDetailComponent],
  providers: [ApodSandbox],
  exports: [APODComponent, ApodDetailComponent],
})
export class ApodModule {}
