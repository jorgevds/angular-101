import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { APOD } from 'src/common/types/interface';
import { Observable } from 'rxjs';
import { ApodSandbox } from '../../sandbox/apod-sandbox.service';
import { map, switchMap } from 'rxjs/operators';
import { add } from 'date-fns';

@Component({
  selector: 'app-apod-detail',
  templateUrl: './apod-detail.component.html',
  styleUrls: ['./apod-detail.component.css'],
})
export class ApodDetailComponent implements OnInit {
  pod$?: Observable<APOD>;
  currentDate$?: Observable<string>;
  previousDate$?: Observable<string>;
  nextDate$?: Observable<string>;

  constructor(
    private ApodSandbox: ApodSandbox,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.setupDateStreams();
    this.getApod();
  }

  private setupDateStreams(): void {
    this.setupCurrentDateStream();
    this.setupPreviousDateStream();
    this.setupNextDateStream();
  }

  private setupCurrentDateStream(): void {
    this.currentDate$ = this.route.paramMap.pipe(
      map((params: ParamMap) => {
        const currentDate: string | null = params.get('id');
        return currentDate != null ? currentDate : '';
      })
    );
  }

  private setupPreviousDateStream(): void {
    if (this.currentDate$ != null) {
      this.previousDate$ = this.currentDate$?.pipe(
        map((value: string) => {
          return add(new Date(value), { days: -1 }).toJSON().slice(0, 10);
        })
      );
    }
  }

  private setupNextDateStream(): void {
    if (this.currentDate$ != null) {
      this.nextDate$ = this.currentDate$?.pipe(
        map((value: string) => {
          return add(new Date(value), { days: 1 }).toJSON().slice(0, 10);
        })
      );
    }
  }

  private getApod(): void {
    if (this.currentDate$ != null) {
      this.pod$ = this.currentDate$.pipe(
        switchMap((value: string) => this.ApodSandbox.getApod(value))
      );
    }
  }
}
