import { Component, OnInit } from '@angular/core';
import { APOD } from 'src/common/types/interface';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { ApodSandbox } from '../../sandbox/apod-sandbox.service';
import { FormService } from 'src/common/services/dateform/date-form.service';
import { FormBuilder } from '@angular/forms';
import { startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-apod',
  templateUrl: './apod.component.html',
  styleUrls: ['./apod.component.css'],
})
export class APODComponent implements OnInit {
  apods$?: Observable<APOD[]>;
  datesForm!: FormGroup;

  constructor(
    private ApodSandbox: ApodSandbox,
    private FormService: FormService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initializeFormValues();
    this.initializeAPODStream();
  }

  initializeFormValues(): void {
    this.datesForm = this.formBuilder.group({
      formFields: this.FormService.buildDateForm(),
    });
  }

  initializeAPODStream(): void {
    this.apods$ = this.datesForm?.valueChanges.pipe(
      startWith(this.datesForm.value),
      switchMap(
        (value: { formFields: { start_date: string; end_date: string } }) =>
          this.ApodSandbox.getRangeOfAPOD(
            value.formFields.start_date,
            value.formFields.end_date
          )
      )
    );
  }
}
