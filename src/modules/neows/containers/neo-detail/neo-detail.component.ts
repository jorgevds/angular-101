import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DetailCometoid } from 'src/common/types/interface';
import { Observable } from 'rxjs';
import { NeowsSandbox } from '../../sandbox/neows-sandbox.service';

@Component({
  selector: 'app-neo-detail',
  templateUrl: './neo-detail.component.html',
  styleUrls: ['./neo-detail.component.css'],
})
export class NeoDetailComponent implements OnInit {
  comet$?: Observable<DetailCometoid>;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private NeowsSandbox: NeowsSandbox
  ) {}

  ngOnInit(): void {
    this.setupCometoid();
  }

  setupCometoid(): void {
    const urlId = String(this.route.snapshot.paramMap.get('id'));
    this.comet$ = this.NeowsSandbox.setupCometoid(urlId);
  }

  goBack(): void {
    this.location.back();
  }
}
