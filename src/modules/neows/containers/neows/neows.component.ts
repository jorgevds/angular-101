import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Neo, Cometoid } from 'src/common/types/interface';
import { FormGroup } from '@angular/forms';
import { NeowsSandbox } from '../../sandbox/neows-sandbox.service';
import { FormService } from 'src/common/services/dateform/date-form.service';
import { FormBuilder } from '@angular/forms';
import { map, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-neows',
  templateUrl: './neows.component.html',
  styleUrls: ['./neows.component.css'],
})
export class NeowsComponent implements OnInit {
  neows$?: Observable<Neo>;
  cometoid$?: Observable<Cometoid>;
  datesForm!: FormGroup;

  constructor(
    private NeowsSandbox: NeowsSandbox,
    private FormService: FormService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initializeFormValues();
    this.setupNeowStream();
    this.setupCometoidStream();
  }

  initializeFormValues(): void {
    this.datesForm = this.formBuilder.group({
      formFields: this.FormService.buildDateForm(),
    });
  }

  setupNeowStream(): void {
    if (this.datesForm != null) {
      this.neows$ = this.datesForm?.valueChanges.pipe(
        startWith(this.datesForm.value),
        switchMap(
          (value: { formFields: { start_date: string; end_date: string } }) =>
            this.NeowsSandbox.getRangeNeows(
              value.formFields.start_date,
              value.formFields.end_date
            )
        )
      );
    }
  }

  setupCometoidStream(): void {
    if (this.neows$) {
      this.cometoid$ = this.neows$.pipe(
        map((value) => value.near_earth_objects)
        // shareReplay() ?
      );
    }
  }
}
