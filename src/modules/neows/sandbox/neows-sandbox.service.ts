import { Injectable } from '@angular/core';
import { NeoService } from '../../../common/services/neows/neo.service';
import { DetailCometoid, Neo } from 'src/common/types/interface';
import { Observable } from 'rxjs';

@Injectable()
export class NeowsSandbox {
  constructor(private NeoService: NeoService) {}

  getRangeNeows(start: string, end: string): Observable<Neo> {
    return this.NeoService.getRangeNeows(start, end);
  }

  setupCometoid(routeId: string): Observable<DetailCometoid> {
    return this.NeoService.getCometoidDetail(routeId);
  }
}
