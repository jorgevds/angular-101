import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NeowsComponent } from './containers/neows/neows.component';
import { NeoDetailComponent } from './containers/neo-detail/neo-detail.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { DateFormModule } from '../../common/modules/dateform/dateform.module';
import { NeowsRoutingModule } from './neows-routing.module';
import { NeowsSandbox } from './sandbox/neows-sandbox.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NeowsRoutingModule,
    NgxSkeletonLoaderModule,
    DateFormModule,
  ],
  declarations: [NeowsComponent, NeoDetailComponent],
  providers: [NeowsSandbox],
  exports: [NeowsComponent, NeoDetailComponent],
})
export class NeowsModule {}
