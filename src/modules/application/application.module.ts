import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ApodModule } from 'src/modules/apod/apod.module';
import { NeowsModule } from 'src/modules/neows/neows.module';
import { AppRoutingModule } from './app-routing.module';
import { ApplicationComponent } from './components/application/application.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from '../../common/components/navbar/navbar.component';
import { DateFormModule } from 'src/common/modules/dateform/dateform.module';

@NgModule({
  declarations: [ApplicationComponent, HomeComponent, NavbarComponent],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    ApodModule,
    NeowsModule,
    DateFormModule,
  ],
  exports: [ApplicationComponent, NavbarComponent],
})
export class ApplicationModule {}
