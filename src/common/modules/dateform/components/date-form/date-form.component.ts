import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { FormService } from '../../../../services/dateform/date-form.service';
import { DateformSandbox } from '../../dateform-sandbox.service';

@Component({
  selector: 'app-date-form',
  templateUrl: './date-form.component.html',
  styleUrls: ['./date-form.component.css'],
})
export class DateFormComponent implements OnInit {
  @Input() formGroupName?: string;
  form!: FormGroup;

  constructor(
    private rootFormGroup: FormGroupDirective,
    public FormService: FormService,
    private DateformSandbox: DateformSandbox
  ) {}

  ngOnInit(): void {
    this.initializeFormValues();
  }

  initializeFormValues(): void {
    if (this.formGroupName) {
      this.form = this.DateformSandbox.initializeFormValues(
        this.rootFormGroup,
        this.formGroupName
      );
    }
  }
}
