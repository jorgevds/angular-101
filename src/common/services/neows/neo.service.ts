import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Neo, DetailCometoid } from '../../types/interface';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class NeoService {
  private baseUrl: string = 'https://api.nasa.gov/neo/rest/v1/';

  constructor(private http: HttpClient) {}
  urlParams = { api_key: environment.NASA_KEY };

  getRangeNeows(start_date: string, end_date: string): Observable<Neo> {
    const requestParams = { start_date, end_date, ...this.urlParams };

    return this.http
      .get<Neo>(this.baseUrl + 'feed?', { params: requestParams })
      .pipe(catchError(this.errorHandler));
  }

  getCometoidDetail(id?: string): Observable<DetailCometoid> {
    return this.http
      .get<DetailCometoid>(this.baseUrl + 'neo/' + id + '?', {
        params: this.urlParams,
      })
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || 'server error');
  }
}
