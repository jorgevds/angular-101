import { Injectable } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  today = new Date().toJSON().slice(0, 10);
  private lastWeek = new Date(Date.now() - 6 * 24 * 60 * 60 * 1000)
    .toJSON()
    .slice(0, 10);
  constructor(private formbuilder: FormBuilder) {}

  buildDateForm(): FormGroup {
    return this.formbuilder.group({
      start_date: this.lastWeek,
      end_date: this.today,
    });
  }
}
