import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { APOD } from 'src/common/types/interface';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class APODService {
  private url: string = 'https://api.nasa.gov/planetary/apod?';
  private headers = new HttpHeaders()
    .set('Access-Control-Allow-Origin', '*')
    .set('Content-Type', 'application/json');
  constructor(private http: HttpClient) {}

  params = { api_key: environment.NASA_KEY };

  getAPOD(date: string | any): Observable<APOD> {
    const requestParams = {
      ...this.params,
      date,
    };

    return this.http
      .get<APOD>(this.url, { params: requestParams, headers: this.headers })
      .pipe(catchError(this.errorHandler));
    // doet http get met juiste url (met juiste start en end dates)
  }
  getRangeOfAPOD(start_date: string, end_date: string): Observable<APOD[]> {
    const requestParams = {
      ...this.params,
      start_date,
      end_date,
    };

    return this.http
      .get<APOD[]>(this.url, { params: requestParams })
      .pipe(catchError(this.errorHandler));
    // doet http get met juiste url (met juiste start en end dates)
  }
  private errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || 'server error');
  }
}
