import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BootstrapComponent } from './component/bootstrap.component';
import { ApplicationModule } from 'src/modules/application/application.module';
import { BootstrapRoutingModule } from './bootstrap-routing.module';

@NgModule({
  declarations: [BootstrapComponent],
  imports: [BrowserModule, ApplicationModule, BootstrapRoutingModule],
  bootstrap: [BootstrapComponent],
})
export class BootstrapModule {}
