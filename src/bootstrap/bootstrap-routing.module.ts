import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('src/modules/application/application.module').then(
        (m) => m.ApplicationModule
      ),
  },
  {
    path: '**',
    loadChildren: () =>
      import('src/modules/application/application.module').then(
        (m) => m.ApplicationModule
      ),
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class BootstrapRoutingModule {}
